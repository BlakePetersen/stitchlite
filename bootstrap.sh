#!/usr/bin/env bash
 
###### -- StitchBlake Vagrant Bootstrap Bash Script
 
 
### Add Dependency Repos
echo '    -=-=- Adding Dependency Repositories and Updating Packages'
 
	sudo add-apt-repository -y ppa:nginx/stable > /dev/null
	wget -O - http://dl.hhvm.com/conf/hhvm.gpg.key | sudo apt-key add -
	echo deb http://dl.hhvm.com/ubuntu trusty main | sudo tee /etc/apt/sources.list.d/hhvm.list

	apt-get update > /dev/null
 
 
### Install Essentials
echo '    -=-=- Installing Essentials...'
 
	apt-get install -y unzip nano git-core curl wget build-essential python-software-properties > /dev/null
 
 
### Install nginx
echo '    -=-=- Installing nginx...'
 
	apt-get -y install nginx > /dev/null
 
 
### Install HHVM
echo '    -=-=- Installing HHVM...'
 
	apt-get install -y hhvm > /dev/null
 
 
### Post-install HHVM scripts
echo '    -=-=- Running HHVM Post-Install Scripts...'
 
	/usr/share/hhvm/install_fastcgi.sh
	update-rc.d hhvm defaults
	service hhvm restart
 
	/usr/bin/update-alternatives --install /usr/bin/php php /usr/bin/hhvm 60
 
 
### Configure Virtual Hosts
echo '    -=-=- Configuring Virtual Hosts...'
 
	# Remove Defaults
	sudo rm /etc/nginx/sites-available/default
	sudo rm /etc/nginx/sites-enabled/default
 
	# Create Virtual Hosts Entry
	echo '

		server {

		    listen 80 default_server;

		    root /vagrant/public;
		    index index.html index.htm index.php;

		    access_log /var/log/nginx/vagrant.access.log;
		    error_log /var/log/nginx/vagrant.error.log;

		    large_client_header_buffers 8 32k;
		 
		    location / {

		        try_files $uri $uri/ /index.php?$query_string;

		    }

		    location = /favicon.ico { log_not_found off; access_log off; }
		    location = /robots.txt  { log_not_found off; access_log off; }

		    error_page 404 /index.php;   

	        include hhvm.conf;

		}

	' > /etc/nginx/sites-available/vagrant
 
	# Symlink into 'Sites Enabled
	ln -s /etc/nginx/sites-available/vagrant /etc/nginx/sites-enabled/vagrant
 
	# Restart nginx
	service nginx stop
	service nginx start
 
	# Add entry to Hosts
	echo '127.0.0.1 vagrant' >> /etc/hosts


### Install MySQL
echo '    -=-=- Installing and Configuring MySQL...'

	sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password DBPewPew!'
	sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password DBPewPew!'
	sudo apt-get -y install mysql-server

	# Mysql stuff

	mysql -hlocalhost -uroot -pDBPewPew! -e 'CREATE DATABASE vagrant_db';
	mysql -hlocalhost -uroot -pDBPewPew! -e 'CREATE USER "vagrant_user"@"localhost" IDENTIFIED BY "DevPewPew!"';
	mysql -hlocalhost -uroot -pDBPewPew! -e 'GRANT ALL PRIVILEGES ON * . * TO "vagrant_user"@"localhost"';
	mysql -hlocalhost -uroot -pDBPewPew! -e 'FLUSH PRIVILEGES';
	mysql -hlocalhost -uroot -pDBPewPew! -e 'SHOW DATABASES';


### Install Composer
echo '    -=-=- Installing and Configuring Composer...'

	curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/local/bin/composer

### Install Laravel
echo '    -=-=- Installing Laravel...'

	composer global require "laravel/installer=~1.1"
	sudo ~/.bashrc >> alias laravel='~/.composer/vendor/bin/laravel'
	sudo source ~/.bashrc


echo '    -=-=- All set, time get to work!' 

