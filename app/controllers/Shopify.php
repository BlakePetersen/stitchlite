<?php 

use \GuzzleHttp\Client;

class Shopify {

	public function getProductData() {
			
		$client = new Client();

		$response = $client->get('https://08ce2c75b4b5967934cb34a5223f9133:4962a725a6e612d1f99a25abd157af77@stitchblake.myshopify.com/admin/products.json');

		$response = $response->json();

		foreach ($response["products"] as $prod) {

			$total_quantity = 0;
			$variantSkus = [];

			foreach ($prod["variants"] as $vari) {

				array_push($variantSkus, $vari["sku"]);
				$total_quantity += $vari["inventory_quantity"];

				$variSearch = array('sku' => $vari["sku"], 'name' => $vari["title"]);

				$variant = Variant::firstOrNew($variSearch);
					
				$variant->name = $vari["title"];
				$variant->sku = $vari["sku"];
				$variant->quantity = $vari["inventory_quantity"];
				$variant->price = $vari["price"];
				$variant->variation = $vari["option1"];

				$variant->save();

			}

			$prodSearch = array('name' => $prod["title"]);

			$product = Product::firstOrNew($prodSearch);
				
			$product->name = $prod["title"];
			$product->variants = json_encode($variantSkus);
			$product->total_quantity = $total_quantity;

			// not ideal, but need to fix the vend qty bug
			$product->shopify = 1;

			$product->save();

		}

	}

}