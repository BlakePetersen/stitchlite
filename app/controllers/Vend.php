<?php

use VendAPI\VendAPI;

class Vend {

	public function getProductData() {

		$vend = new VendAPI('https://stitchblake.vendhq.com','blakepetersen@gmail.com','AW6PkroXPx^p');
		$products = $vend->getProducts();

		$count = count($products);

		$qtyBySku = [];

		for ($i=0; $i < $count; $i++) { 

			// Products
			if($products[$i]->composites !== NULL) {
				
				$total_quantity = 0;
				$variantSkus = [];

				$product = Product::where('name' ,'=', $products[$i]->name)->first();

				/* -- Vend-only sku is adding too many to qty -- */
				if ($product && $product->vend !== 1) {
					$total_quantity= $product->total_quantity;
				}

				foreach ($products[$i]->composites as $vari) {

					$qtyBySku[$vari->sku] = $vari->count;
					array_push($variantSkus, $vari->sku);
					$total_quantity += $vari->count;

				}

				$product = Product::firstOrNew(array('name' => $products[$i]->name));
					
				$product->name = $products[$i]->name;
				$product->variants = json_encode($variantSkus);
				$product->total_quantity = $total_quantity;

				// not ideal, but need to fix the vend qty bug
				$product->vend = 1;

				$product->save();
	

			// Variants
			} elseif ($products[$i]->price !== 0)  {

				$variant = Variant::firstOrNew(array('sku' => $products[$i]->sku));
					
				$variant->name = $products[$i]->name;
				$variant->sku = $products[$i]->sku;
				$variant->price = $products[$i]->price;
				$variant->variation = $products[$i]->variant_option_one_value;

				$variant->save();

			}

		}

		/* -- Last minute fix to address the difficulties of directly grabbing the inventory count from the API -- */

		foreach ($qtyBySku as $sku => $qty) {
			
			// Wrap up with call to add qty to variants, not sure why "Array/Object" monster was being such a headache

			$variant = Variant::firstOrNew(array('sku' => $sku));
				
			$variant->quantity = $qty;

			$variant->save();

		}

	}

}