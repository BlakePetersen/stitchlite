<?php 

class SyncController extends BaseController {

	public function run() {

		$shopifyData = new Shopify;
		$shopifyDataJSON = $shopifyData->getProductData();

		$vendData = new Vend;
		$vendDataJSON = $vendData->getProductData();

		return Redirect::to('/')->with('message', 'Sync Successful');

	}

	public function purge() {

		Product::where('id', '>', 0)->delete();
		Variant::where('id', '>', 0)->delete();

		return Redirect::to('/')->with('message', 'Purge Successful');

	}

}