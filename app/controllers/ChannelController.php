<?php 



class ChannelController extends BaseController {


	public function getChannelJSON($channel) {

		$channel = Channel::where('name' ,'=', $channel)->first();

		$json = 'https://'. $channel->key . ':' . $channel->pw . '@' . $channel->url . $channel->path;

		$channel->json = $json;

		return $channel;

	}


	public function index() {

		$channels = Channel::all();
		return View::make('pages/channels/index', compact('channels'));

	}

	public function show($channel) {

		// Tack on the JSON URL to the $channel object
		$channel = $this->getChannelJSON($channel);

		// Display channel details
		return View::make('pages/channels/show', compact('channel'));

	}


	public function create() {

		return View::make('pages/channels/create');

	}


}