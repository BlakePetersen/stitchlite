<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/* -- Home UI -- */
Route::get('/','HomeController@index');

/* -- Sync Data -- */
Route::get('sync','SyncController@run');

/* -- Purge Data -- */
Route::get('purge','SyncController@purge');



/* -- Channel Resource -- */
Route::resource('channel', 'ChannelController');

/* -- Product Resource -- */
Route::resource('product', 'ProductController');

/* -- Variant Resource -- */
Route::resource('variant', 'VariantController');