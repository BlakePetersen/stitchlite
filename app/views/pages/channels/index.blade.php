@extends('templates.base')

@section('body/main')
	<div>

		<h2>StitchBlake</h2>

		@foreach ($channels as $channel)

			<li><a href="/channel/{{ $channel->name }}" title="View {{ $channel->name }}">{{ $channel->name }}</a></li>

		@endforeach

	</div>
@stop