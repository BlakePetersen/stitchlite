@extends('templates.base')

@section('body/main')
	<div>

		<h2>{{ $channel->name }}</h2>
		<p>{{ $channel->id }}</p>
		<p>{{ $channel->created_at }}</p>
		<p>{{ $channel->updated_at }}</p>
		<p>{{ $channel->key }}</p>
		<p>{{ $channel->pw }}</p>
		<p>{{ $channel->secret }}</p>
		<p>{{ $channel->url }}</p>
		<p>{{ $channel->path }}</p>

		<a href="{{ $channel->json }}">JSON</a>

	</div>
@stop