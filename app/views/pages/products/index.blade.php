@extends('templates.base')

@section('body/main')

	<h2>All Products</h2>

	<div>

		<table class=" table table-striped">
			
			<thead>
				<tr>
					<td>Sku</td>
					<td>Name</td>
					<td>Total Qty</td>
					<td>Price</td>
					<td>Image</td>
				</tr>
			</thead>
			<tbody>

				@foreach ($products as $product)

					<tr>			
						<td>{{ $product->name }}</td>
						<td><a href="/product/{{ $product->id }}" title="View {{ $product->name }}">{{ $product->name }}</a></td>
						<td>{{ $product->total_quantity }}</td>
						<td>Price</td>
						<td>Image</td>
						
					</tr>

				@endforeach

			</tbody>

		</table>

	</div>

@stop