@extends('templates.base')

@section('body/main')
	<div>

		<h2>{{ $product->name }}</h2>
		<p>id: {{ $product->id }}</p>
		<p>created_at: {{ $product->created_at }}</p>
		<p>updated_at: {{ $product->updated_at }}</p>

		<p>qty: {{ $product->total_quantity }}</p>

		<p>variants: {{ $product->variants }}</p>

	</div>
@stop