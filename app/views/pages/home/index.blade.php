@extends('templates.base')

@section('body/main')

	<div>

		<a href="/purge" class="btn btn-danger pull-right">Purge Data</a>

		<a href="/sync" class="btn btn-primary pull-right">Update Data</a>

		<h2>All Products &nbsp; <small><a href="/variant">All Variants</a></small></h2>

	</div>

	<hr />

	<table class="table table-striped">
		
		<thead>
			<tr>
				<td>Image</td>
				<td>Name</td>
				<td>Total Qty</td>
			</tr>
		</thead>
		<tbody>

			@foreach ($products as $product)

				<tr>		
					<td></td>	
					<td><a href="/product/{{ $product->id }}" title="View {{ $product->name }}">{{ $product->name }}</a></td>
					<td>{{ $product->total_quantity }}</td>
				</tr>
				<!-- 
				<tr>
					<td colspan="4">				
						@{{ $product->getVariants($product->variants) }}
					</td>
				</tr>
	-->
			@endforeach

		</tbody>

	</table>

@stop