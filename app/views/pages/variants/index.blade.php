@extends('templates.base')

@section('body/main')

	<div>

		<a href="/purge" class="btn btn-danger pull-right">Purge Data</a>

		<a href="/sync" class="btn btn-primary pull-right">Update Data</a>

		<h2>Variant Page  &nbsp; <small><a href="/">All Products</a></small></h2>

	</div>

	<hr>

	<div>

		<table class="table table-striped">
			
			<thead>
				<tr>
					<td>Sku</td>
					<td>Name</td>
					<td>Total Qty</td>
					<td>Price</td>
				</tr>
			</thead>
			<tbody>

				@foreach ($variants as $variant)

					<tr>			
						<td>{{ $variant->sku }}</td>
						<td><a href="/variant/{{ $variant->id }}" title="View {{ $variant->name }}">{{ $variant->name }}</a></td>
						<td>{{ $variant->quantity }}</td>
						<td>{{ $variant->price }}</td>
						
					</tr>

				@endforeach

			</tbody>

		</table>

	</div>

@stop