<!doctype html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>StitchBlake</title>

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="/assets/js/req.js"></script>    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.11/require.min.js"></script>
    <script type="text/javascript" src="/assets/js/app.js"></script>

</head>
<body>
	<div class="container">

		<div class="row">
			
			<header class="text-center">

				<h1><a href="/">StitchBlake</a><br /><small>Merges data from <a href="http://stitchblake.myshopify.com/">Shopify (password 'frieff')</a> and <a href="http://stitchblake.vendhq.com/">Vend</a></small></h1>

			</header>

			@yield('body/main')

		</div>

	</div>
</body>
</html>
</body>
</html>
