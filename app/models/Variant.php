<?php

class Variant extends Eloquent {

	protected $table = 'Variants';

	protected $fillable = ['sku','name'];

}