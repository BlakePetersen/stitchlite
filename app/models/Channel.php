<?php 

class Channel extends Eloquent {

	protected $table = 'Channels';

	protected $fillable = ['name', 'key', 'pw', 'secret', 'url', 'path'];

}